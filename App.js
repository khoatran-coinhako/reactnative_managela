/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';


import Viewport from './app/Viewport';

export default class App extends Component<{}> {
  render() {
    return (
      <View>
        <Text>Hello World</Text>
      </View>
    );
  }
}

AppRegistry.registerComponent('App', () => Viewport);